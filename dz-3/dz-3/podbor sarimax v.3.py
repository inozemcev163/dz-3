import numpy as np
from statsmodels.tsa.statespace import sarimax
import pandas as pd
import matplotlib.pyplot as plt 
from statsmodels.tsa.statespace.sarimax import SARIMAX
import itertools
import warnings
import statsmodels.api as sm
from contextlib import suppress


from numba import jit





dataset = pd.read_csv("wine_Australia.tsv", sep = '\t')
#print(dataset.head())


time_series = dataset['red'].values

#plt.plot(time_series, color = 'blue')

# Определите p, d и q в диапазоне 0-2
p = d = q = range(0, 10)

maxranges = 10
qs = range(0, maxranges)
ds = range(maxranges-1 ,maxranges))#без этого x[2] не пеходит на следующее значение !!!!!!!!!!!!!!!!!!!!!!!!
ps = range(5,maxranges)#ограниченный поиск лучших значений
# Сгенерируйте различные комбинации p, q и q
pdq = list(itertools.product(p, d, q))
pdqs = list(itertools.product(ps, ds, qs)) #создаем отдельный лист для seasonal значений
# Сгенерируйте комбинации сезонных параметров p, q и q
#seasonal_pdq = [(x[0], x[1], x[2], 12) for x in list(itertools.product(p, d, q))] - изначально, затем по значениям подобрать и заменить иксы на подходящие цифры
seasonal_pdq = [(x[0], 6, x[2], 12) for x in list(itertools.product(ps, ds, qs))]

@jit
def bb():
    warnings.filterwarnings("ignore") # отключает предупреждения
    model = sm.tsa.statespace.SARIMAX(time_series, order=param, seasonal_order=param_seasonal, enforce_stationarity=False, enforce_invertibility=False)
    results = model.fit()
    print('ARIMA{}x{}12 - AIC:{}'.format(param, param_seasonal, results.aic))

for param in pdq:
    for param_seasonal in seasonal_pdq:
        try:
            bb()
        except:
            pass
x = input()
print(x)
#pdq = (0, 1, 0)
#PDQS = (1, 0, 0, 12)
#model = SARIMAX(time_series, order = pdq, seasonal_order = PDQS)

#тренируем модель и делаем предсказание
#model_fit = model.fit()
#predicted = model_fit.predict(start=1, end=len(time_series) + 8) #метод predict принимет два параметра - когда начать предсказывать и когда пора остановиться. Здесь мы делаем предсказание на 10 месяцев вперед

#смотрим на наше предсказание
#plt.plot(predicted, color = 'red')
#plt.show()

